//TODO implement Data Access Layer

var mongoose = require('mongoose');

/**
 * Data Access Layer
 *
 * @constructor
 * @param {Object} config - database config
 */
function DAO(config) {
    var mongoDB = 'mongodb://'+config.userName+':'+config.userPassword+'@'+config.url+':'+config.port+'/'+config.name;
    //var mongoDB = 'mongodb://db_user:1q2w3e4r5t6y@ds145283.mlab.com:45283/mydb';
    mongoose.connect(mongoDB);
    mongoose.Promise = global.Promise;
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));
}

/**
 * Create database instance and load init data
 * @param {Object} data - init database data
 * @param {Function} callback - two params err, callback result
 * @returns {void }
 */
DAO.prototype.init = function (data, callback) {
    //TODO create instance and load data
    callback && callback();
};

/**
 * Clear database
 * @param {Function} callback - two params err, callback result
 * @returns {void}
 */
DAO.prototype.clear = function(callback) {
    //TODO clear database
    callback && callback();
};

module.exports = DAO;